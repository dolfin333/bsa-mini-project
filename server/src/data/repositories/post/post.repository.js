import { Op } from 'sequelize';
import { sequelize } from '../../db/connection';
import { Abstract } from '../abstract/abstract.repository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

const likeQuery = bool => `(SELECT ARRAY(SELECT "userId" FROM "postReactions"
WHERE "postReactions"."isLike" = ${bool} AND "post"."id" = "postReactions"."postId"))`;

class Post extends Abstract {
  constructor({
    postModel,
    commentModel,
    userModel,
    imageModel,
    postReactionModel
  }) {
    super(postModel);
    this._commentModel = commentModel;
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._postReactionModel = postReactionModel;
  }

  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      isOwnPosts,
      isLikedPosts,
      userId
    } = filter;
    const where = {};
    let likedPosts = {};

    if (userId) {
      if (isOwnPosts.toString() === 'true') {
        Object.assign(where, { userId });
      }
      if (isOwnPosts.toString() === 'false') {
        Object.assign(where, { userId: { [Op.not]: userId } });
      }
      if (isLikedPosts.toString() === 'true') {
        likedPosts = (await this._postReactionModel.findAll({
          where: { userId },
          attributes: ['postId', 'isLike']
        })).map(postReaction => (
          postReaction.dataValues.isLike ? postReaction.dataValues.postId : null)).filter(id => (id !== null));
        Object.assign(where, { id: likedPosts });
      }
    }
    let posts = await this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" 
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(likeQuery(true)), 'likeUsers'],
          [sequelize.literal(likeQuery(false)), 'dislikeUsers']
        ]
      },
      include: [{
        model: this._imageModel,
        attributes: ['id', 'link']
      }, {
        model: this._userModel,
        attributes: ['id', 'username'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: this._postReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
    posts = await Promise.all(posts.map(async post => {
      post.dataValues.likeUsers = await this.getUsers(post.dataValues.likeUsers);
      post.dataValues.dislikeUsers = await this.getUsers(post.dataValues.dislikeUsers);
      return post;
    }));
    // console.log(posts);
    return posts;
  }

  async getPostById(id) {
    const post = await this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" 
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount'],
          [sequelize.literal(likeQuery(true)), 'likeUsers'],
          [sequelize.literal(likeQuery(false)), 'dislikeUsers']
        ]
      },
      include: [{
        model: this._commentModel,
        attributes: {
          include: [
            [sequelize.literal(`
          (SELECT COUNT(*)
          FROM "commentReactions"
          WHERE "comments"."id" = "commentReactions"."commentId" 
          AND "commentReactions"."isLike"=true)`), 'likeCountComment'],
            [sequelize.literal(`
          (SELECT COUNT(*)
          FROM "commentReactions"
          WHERE "comments"."id" = "commentReactions"."commentId" 
          AND "commentReactions"."isLike"=false)`), 'dislikeCountComment'],
            [sequelize.literal(`
          (SELECT ARRAY(SELECT "userId"
          FROM "commentReactions"
          WHERE "commentReactions"."isLike" = true 
          AND "comments"."id" = "commentReactions"."commentId"))`), 'likeUsersComment'],
            [sequelize.literal(`
          (SELECT ARRAY(SELECT "userId"
          FROM "commentReactions"
          WHERE "commentReactions"."isLike" = false 
          AND "comments"."id" = "commentReactions"."commentId" ))`), 'dislikeUsersComment']
          ]
        },
        include: {
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        }
      }, {
        model: this._userModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: this._imageModel,
        attributes: ['id', 'link']
      }, {
        model: this._postReactionModel,
        attributes: []
      }]
    });
    if (post) {
      post.dataValues.comments = await Promise.all(post.dataValues.comments.map(async comment => {
        comment.dataValues.likeUsersComment = await this.getUsers(comment.dataValues.likeUsersComment);
        comment.dataValues.dislikeUsersComment = await this.getUsers(comment.dataValues.dislikeUsersComment);
        return comment;
      }));
      post.dataValues.likeUsers = await this.getUsers(post.dataValues.likeUsers);
      post.dataValues.dislikeUsers = await this.getUsers(post.dataValues.dislikeUsers);
    }
    return post;
  }

  async getUsers(users) {
    const resUsers = await Promise.all(users.map(async user => {
      const resUser = await this._userModel.findOne({
        group: ['user.id', 'image.id'],
        attributes: ['id', 'username', 'status'],
        where: { id: user },
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      });
      return resUser;
    }));
    return resUsers;
  }
}

export { Post };
