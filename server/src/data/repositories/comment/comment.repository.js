import { sequelize } from '../../db/connection';
import { Abstract } from '../abstract/abstract.repository';

// const likeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class Comment extends Abstract {
  constructor({ commentModel, userModel, imageModel, commentReactionModel }) {
    super(commentModel);
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._commentReactionModel = commentReactionModel;
  }

  getCommentById(id) {
    return this.model.findOne({
      group: ['comment.id', 'user.id', 'user->image.id'],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
              (SELECT COUNT(*) FROM "commentReactions"
              WHERE "commentReactions"."commentId"='${id}' 
              AND "commentReactions"."isLike"=true)`), 'likeCountComment'],
          [sequelize.literal(`
        (SELECT COUNT(*) FROM "commentReactions"
        WHERE "commentReactions"."commentId"='${id}'
        AND "commentReactions"."isLike"=false)`), 'dislikeCountComment'],
          [sequelize.literal(`
        (SELECT ARRAY(SELECT "userId"
        FROM "commentReactions"
        WHERE "commentReactions"."isLike" = true AND "commentReactions"."commentId"='${id}'))`), 'likeUsersComment'],
          [sequelize.literal(`
        (SELECT ARRAY(SELECT "userId"
        FROM "commentReactions"
        WHERE "commentReactions"."isLike" = false AND "commentReactions"."commentId"='${id}'))`), 'dislikeUsersComment']
        ]
      },
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: this._commentReactionModel,
          attributes: []
        }
      ]
    });
  }
}

export { Comment };
