import {
  CommentModel,
  UserModel,
  ImageModel,
  PostModel,
  PostReactionModel,
  CommentReactionModel
} from '../models';
import { Comment } from './comment/comment.repository';
import { Image } from './image/image.repository';
import { PostReaction } from './post-reaction/post-reaction.repository';
import { CommentReaction } from './comment-reaction/comment-reaction.repository';
import { Post } from './post/post.repository';
import { User } from './user/user.repository';

const comment = new Comment({
  commentModel: CommentModel,
  userModel: UserModel,
  imageModel: ImageModel,
  commentReactionModel: CommentReactionModel
});

const commentReaction = new CommentReaction({
  commentReactionModel: CommentReactionModel,
  commentModel: CommentModel
});

const image = new Image({
  imageModel: ImageModel
});

const postReaction = new PostReaction({
  postReactionModel: PostReactionModel,
  postModel: PostModel
});

const post = new Post({
  postModel: PostModel,
  commentModel: CommentModel,
  userModel: UserModel,
  imageModel: ImageModel,
  postReactionModel: PostReactionModel
});

const user = new User({
  userModel: UserModel,
  imageModel: ImageModel
});

export { comment, image, postReaction, commentReaction, post, user };
