import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (Router, services) => {
  const { comment: commentService } = services;
  const router = Router();

  router
    .get(CommentsApiPath.$ID, (req, res, next) => commentService
      .getCommentById(req.params.id)
      .then(comment => res.send(comment))
      .catch(next))
    .post(CommentsApiPath.ROOT, (req, res, next) => commentService
      .create(req.user.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .post(CommentsApiPath.LIKE, (req, res, next) => commentService
      .setLike(req.user.id, req.body)
      .then(reaction => {
        if (reaction.comment && reaction.comment.userId !== req.user.id) {
          // notify a user if someone (not himself) liked his com
          req.io
            .to(reaction.comment.userId)
            .emit('like', 'Your comments was liked!');
        }
        return res.send(reaction);
      })
      .catch(next))
    .put(CommentsApiPath.$ID, (req, res, next) => commentService
      .updateComment(req.params.id, req.body)
      .then(comment => {
        req.io.emit('updated_comment', comment); // notify all users that a comment was updated
        return res.send(comment);
      })
      .catch(next))
    .delete(CommentsApiPath.$ID, (req, res, next) => commentService
      .deleteComment(req.params.id)
      .then(comment => {
        req.io.emit('deleted_comment', comment); // notify all users that a comment was deleted
        return res.send(comment);
      })
      .catch(next))
    .post(CommentsApiPath.DISLIKE, (req, res, next) => commentService
      .setDislike(req.user.id, req.body)
      .then(reaction => {
        if (reaction.comment && reaction.comment.userId !== req.user.id) {
          // notify a user if someone (not himself) disliked his com
          req.io
            .to(reaction.comment.userId)
            .emit('dislike', 'Your comment was disliked!');
        }
        return res.send(reaction);
      })
      .catch(next));
  return router;
};

export { initComment };
