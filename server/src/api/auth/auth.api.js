import { AuthApiPath } from '../../common/enums/enums';
import {
  authentication as authenticationMiddleware,
  registration as registrationMiddleware,
  jwt as jwtMiddleware,
  changePassword as changePasswordMiddleware
} from '../../middlewares/middlewares';

const initAuth = (Router, services) => {
  const { auth: authService, user: userService } = services;
  const router = Router();

  // user added to the request (req.user) in a strategy, see passport config
  router
    .post(AuthApiPath.PASSWORD, changePasswordMiddleware, (req, res, next) => authService
      .changePassword(req.body)
      .then(data => res.send(data))
      .catch(next))
    .post(AuthApiPath.LOGIN, authenticationMiddleware, (req, res, next) => authService
      .login(req.user)
      .then(data => res.send(data))
      .catch(next))
    .post(AuthApiPath.REGISTER, registrationMiddleware, (req, res, next) => authService
      .register(req.user)
      .then(data => res.send(data))
      .catch(next))
    .get(AuthApiPath.USER + AuthApiPath.$ID, jwtMiddleware, (req, res, next) => userService
      .getUserById(req.params.id)
      .then(data => res.send(data))
      .catch(next))
    .put(AuthApiPath.USER + AuthApiPath.$ID, (req, res, next) => userService
      .updateUser(req.params.id, req.body)
      .then(user => {
        req.io.emit('updated_profile', user); // notify all users that a post was updated
        return res.send(user);
      })
      .catch(next))
    .get(AuthApiPath.USER, jwtMiddleware, (req, res, next) => userService
      .getUserById(req.user.id)
      .then(data => res.send(data))
      .catch(next))
    .get(AuthApiPath.USER + AuthApiPath.USERNAME + AuthApiPath.$USERNAME, (req, res, next) => authService
      .getUserByUsername(req.params.username)
      .then(data => res.send(data))
      .catch(next));

  return router;
};

export { initAuth };
