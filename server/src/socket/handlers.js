const handlers = socket => {
  // socket.emit('Hi', 'Socket!!');
  socket.on('createRoom', roomId => {
    socket.join(roomId);
  });
  socket.on('leaveRoom', roomId => {
    socket.leave(roomId);
  });
};

export { handlers };
