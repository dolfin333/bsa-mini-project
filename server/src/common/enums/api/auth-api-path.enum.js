const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  PASSWORD: '/password',
  $USERNAME: '/:username',
  USERNAME: '/username',
  $ID: '/:id'
};

export { AuthApiPath };
