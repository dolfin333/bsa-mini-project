const CommentsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  LIKE: '/like',
  DISLIKE: '/dislike'
};

export { CommentsApiPath };
