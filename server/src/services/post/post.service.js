class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  createPost(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  async deletePost(postId) {
    const post = await this._postRepository.getPostById(postId);
    await this._postRepository.deleteById(postId);
    return post;
  }

  updatePost(postId, post) {
    return this._postRepository.updateById(postId, { ...post });
  }

  async setLike(userId, { postId, isLike = true }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction(userId, postId);
  }

  async setDislike(userId, { postId, isLike = false }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction(userId, postId);
  }
}

export { Post };
