import { encrypt, createToken } from '../../helpers/helpers';

class Auth {
  constructor({ userRepository }) {
    this._userRepository = userRepository;

    this.register = this.register.bind(this);
  }

  async login({ id }) {
    return {
      token: createToken({ id }),
      user: await this._userRepository.getUserById(id)
    };
  }

  async register({ password, ...userData }) {
    const newUser = await this._userRepository.addUser({
      ...userData,
      password: await encrypt(password)
    });

    return this.login(newUser);
  }

  async getUserByUsername(username) {
    const user = await this._userRepository.getByUsername(username);
    if (!user) {
      return {};
    }
    return user;
  }

  async changePassword({ username, password }) {
    password = await encrypt(password);
    const newUser = await this._userRepository.updatePasswordByUsername(username, password);
    return newUser;
  }
}

export { Auth };
