import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType } from 'src/common/enums/enums';
import { Button, Form, SemanticButton } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';

import styles from './styles.module.scss';

const UpdateComment = ({ onCommentUpdate, commentToUpdate }) => {
  const [body, setBody] = React.useState(commentToUpdate.body);

  const handleUpdateComment = async () => {
    if (!body) {
      return;
    }
    await onCommentUpdate({ body, id: commentToUpdate.id });
  };
  const handleCloseUpdateComment = async () => {
    if (!body) {
      return;
    }
    await onCommentUpdate({ body: commentToUpdate.body, id: commentToUpdate.id });
  };

  return (
    <Form onSubmit={handleUpdateComment}>
      <Form.TextArea
        name="body"
        value={body}
        onChange={ev => setBody(ev.target.value)}
      />
      <div className={styles.btnWrapper}>
        <SemanticButton.Group>
          <Button onClick={handleCloseUpdateComment}>Cancel</Button>
          <SemanticButton.Or />
          <SemanticButton
            color={ButtonColor.BLUE}
            type={ButtonType.SUBMIT}
          >
            Update
          </SemanticButton>
        </SemanticButton.Group>
      </div>
    </Form>
  );
};

UpdateComment.propTypes = {
  onCommentUpdate: PropTypes.func.isRequired,
  commentToUpdate: commentType.isRequired
};

export default UpdateComment;
