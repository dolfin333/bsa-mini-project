import * as React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { IconName, IconColor, ENV } from 'src/common/enums/enums';
import { Icon, Modal, Input, Message } from 'src/components/common/common';
import { send } from 'emailjs-com';
import validator from 'validator';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close }) => {
  const [isCopied, setIsCopied] = React.useState(false);
  const [mess, setMess] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [isEmailValid, setIsEmailValid] = React.useState(true);
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  let input = React.useRef();

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
    setMess('');
  };

  const copyToClipboard = ({ target }) => {
    input.select();
    document.execCommand('copy');
    target.focus();
    setIsCopied(true);
  };

  const handleSendEmailClick = e => {
    e.preventDefault();
    // setLoading(true);
    send(
      ENV.SERVICE_ID,
      ENV.TEMPLATE_ID_SHARE_POST,
      { email, postLink: `${window.location.origin}/share/${postId}`, username: user.username },
      ENV.USER_ID
    )
      .then(() => {
        // console.log('SUCCESS!', response.status, response.text);
        setMess(`Send it to email ${email}!`);
        setIsEmailValid(true);
      })
      .catch(() => {
        // console.log('FAILED...', err.text);
        setIsEmailValid(false);
      });
    // setLoading(false);
  };

  return (
    <Modal open closeOnDimmerClick onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {isCopied && (
          <span>
            <Icon name={IconName.COPY} color={IconColor.GREEN} />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={ref => {
            input = ref;
          }}
        />
        <br />
        <Input
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'send',
            content: 'Send post',
            onClick: handleSendEmailClick
          }}
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
          ref={ref => {
            input = ref;
          }}
        />
        {mess !== '' && (
          <Message>
            {mess}
          </Message>
        )}
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default SharedPostLink;
