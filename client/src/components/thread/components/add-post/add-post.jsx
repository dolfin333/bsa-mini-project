import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Button, Form, Image, Segment, Message, SemanticButton } from 'src/components/common/common';

import styles from './styles.module.scss';

const AddPost = ({ onPostAdd, uploadImage }) => {
  const [body, setBody] = React.useState('');
  const [image, setImage] = React.useState(undefined);
  const [isUploading, setIsUploading] = React.useState(false);
  const [errImgMess, setErrImgMess] = React.useState('');

  const handleAddPost = async () => {
    if (!body) {
      return;
    }
    await onPostAdd({ imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
    setErrImgMess('');
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
        setErrImgMess('');
      })
      .catch(() => {
        setErrImgMess('Something wrong with img');
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Segment>
      <Form onSubmit={handleAddPost}>
        {errImgMess !== '' && (
          <Message negative>
            <Message.Header>{errImgMess}</Message.Header>
          </Message>
        )}
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => { setBody(ev.target.value); setErrImgMess(''); }}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post_img" />
          </div>
        )}
        <div className={styles.btnWrapper}>
          <SemanticButton.Group>
            <Button onClick={() => setImage(null)}>Delete image</Button>
            <SemanticButton.Or />
            <Button
              color="teal"
              isLoading={isUploading}
              isDisabled={isUploading}
              iconName={IconName.IMAGE}
            >
              <label className={styles.btnImgLabel}>
                Attach image
                <input
                  name="image"
                  type="file"
                  onChange={handleUploadFile}
                  hidden
                />
              </label>
            </Button>
          </SemanticButton.Group>
          <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
            Post
          </Button>
        </div>
      </Form>
    </Segment>
  );
};

AddPost.propTypes = {
  onPostAdd: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default AddPost;
