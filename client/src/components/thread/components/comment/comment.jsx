import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { IconName } from 'src/common/enums/enums';
import {
  Comment as CommentUI, Label, Icon, Dropdown,
  Confirm, Popup, Modal, Image
} from 'src/components/common/common';
import UpdateComment from 'src/components/thread/components/update-comment/update-comment';
import { commentType } from 'src/common/prop-types/prop-types';
import { useSelector } from 'react-redux';

import styles from './styles.module.scss';

const Comment = ({ comment, onCommentLike, onCommentDislike,
  onUpdatedCommentToggle, onDeleteComment, onCommentUpdate }) => {
  const { userId, commentToUpdate } = useSelector(state => ({
    userId: state.profile.user.id,
    commentToUpdate: state.posts.commentToUpdate
  }));
  const {
    id,
    body,
    user,
    likeCountComment,
    dislikeCountComment,
    likeUsersComment,
    dislikeUsersComment,
    createdAt,
    updatedAt
  } = comment;
  const date = getFromNowTime(updatedAt);
  const [open, setOpen] = React.useState(false);
  const [openModal, setOpenModal] = React.useState(false);
  const [usersReacted, setUsersReacted] = React.useState([]);

  const handleOpenUsersLike = () => { setOpenModal(true); setUsersReacted(likeUsersComment); };
  const handleOpenUsersDislike = () => { setOpenModal(true); setUsersReacted(dislikeUsersComment); };

  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);
  const handleUpdatedCommentToggle = () => onUpdatedCommentToggle(id);

  const handleConfirm = () => { setOpen(false); onDeleteComment(id); };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <div className={styles.meta}>
          <div>
            <span style={{ display: 'flex' }}>
              <CommentUI.Author as="a">
                <span style={{ display: 'grid' }}>
                  {user.username}
                </span>
              </CommentUI.Author>
              <CommentUI.Metadata>
                {updatedAt === createdAt ? 'commented at' : 'updated at'}
                {' - '}
                {date}
              </CommentUI.Metadata>
            </span>
            <div className={styles.userStatusWrapper}>
              <span className={styles.userStatus}>{user.status}</span>
            </div>
          </div>
          {userId === user.id && (
            <Dropdown icon={IconName.ELLIPSIS_HORIZONTAL} pointing="right">
              <Dropdown.Menu>
                <Dropdown.Item text="Update comment" onClick={handleUpdatedCommentToggle} />
                <Dropdown.Item text="Delete comment" onClick={() => setOpen(true)} />
              </Dropdown.Menu>
            </Dropdown>
          )}
        </div>
        <CommentUI.Text>
          {commentToUpdate && id === commentToUpdate.id
            ? <UpdateComment commentToUpdate={commentToUpdate} onCommentUpdate={onCommentUpdate} /> : body}
        </CommentUI.Text>
      </CommentUI.Content>
      <CommentUI.Content extra="true">
        <Popup
          className={styles.popup}
          trigger={(
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handleCommentLike}
            >
              <Icon name={IconName.THUMBS_UP} />
              {likeCountComment}
            </Label>
          )}
          content={(likeUsersComment.length !== 0 && (
            <div className={styles.popup_reaction}>
              {likeUsersComment.slice(-4).map(likeUserComment => (
                <span key={likeUserComment.id + likeUserComment.username}>
                  <Image src={likeUserComment.image?.link ?? DEFAULT_USER_AVATAR} avatar style={{ margin: '5px' }} />
                  <p>{likeUserComment.username}</p>
                </span>
              ))}
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={handleOpenUsersLike}
              >
                <Icon name={IconName.PLUS} />
              </Label>
            </div>
          )) || 'no users'}
          basic
          hoverable
        />
        <Popup
          className={styles.popup}
          trigger={(
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handleCommentDislike}
            >
              <Icon name={IconName.THUMBS_DOWN} />
              {dislikeCountComment}
            </Label>
          )}
          content={(dislikeUsersComment.length !== 0 && (
            <div className={styles.popup_reaction}>
              {dislikeUsersComment.map(dislikeUserComment => (
                <span key={dislikeUserComment.id + dislikeUserComment.username}>
                  <Image
                    src={dislikeUserComment.image?.link ?? DEFAULT_USER_AVATAR}
                    avatar
                    alt="user_img"
                    style={{ margin: '5px' }}
                  />
                  <p>{dislikeUserComment.username}</p>
                </span>
              ))}
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={handleOpenUsersDislike}
              >
                <Icon name={IconName.PLUS} />
              </Label>
            </div>
          )) || 'no users'}
          basic
          hoverable
        />
        <Modal
          dimmer="blurring"
          centered={false}
          open={openModal}
          onClose={() => setOpenModal(false)}
        >
          {usersReacted && (
            <Modal.Content>
              {usersReacted.map(userReacted => (
                <span key={userReacted.id + userReacted.username} className={styles.modal_info}>
                  <Image
                    src={userReacted.image?.link ?? DEFAULT_USER_AVATAR}
                    size="tiny"
                    circular
                    style={{ marginRight: '0' }}
                    alt="user_img"
                  />
                  <span>
                    <span className={styles.modal_user_info}>
                      {userReacted.username}
                      <span style={{ color: 'gray' }}>{userReacted.status}</span>
                    </span>
                  </span>
                </span>
              ))}
            </Modal.Content>
          )}
        </Modal>
      </CommentUI.Content>
      <Confirm
        open={open}
        header="Comment deleting"
        content="Are you sure that you want to delete this comment?"
        confirmButton="Delete"
        onCancel={() => { setOpen(false); }}
        onConfirm={handleConfirm}
      />
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onUpdatedCommentToggle: PropTypes.func.isRequired,
  onDeleteComment: PropTypes.func.isRequired,
  onCommentUpdate: PropTypes.func.isRequired
};

export default Comment;
