import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI } from 'src/components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  onUpdatedPostToggle, uploadImage, sharePost, onPostUpdate, onDeletePost,
  onPostLike, onPostDislike, onExpandedPostToggle
}) => {
  const dispatch = useDispatch();
  const { post, postComments } = useSelector(state => ({
    post: state.posts.expandedPost,
    postComments: state.posts.expandedPostComments
  }));

  const handleCommentLike = React.useCallback(id => (
    dispatch(threadActionCreator.likeComment(id))
  ), [dispatch]);

  const handleCommentDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikeComment(id))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleUpdatedCommentToggle = React.useCallback(id => {
    dispatch(threadActionCreator.toggleUpdatedComment(id));
  }, [dispatch]);

  const handleCommentUpdate = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.updateComment(commentPayload))
  ), [dispatch]);

  const handleDeleteComment = React.useCallback(id => {
    dispatch(threadActionCreator.deleteComment(id));
  }, [dispatch]);

  const handleExpandedPostClose = () => onExpandedPostToggle();

  const sortedComments = getSortedComments(postComments ?? []);
  return (
    <Modal
      // dimmer="blurring"
      centered={false}
      open
      closeOnDimmerClick
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            onPostLike={onPostLike}
            onPostDislike={onPostDislike}
            onExpandedPostToggle={onExpandedPostToggle}
            onUpdatedPostToggle={onUpdatedPostToggle}
            onDeletePost={onDeletePost}
            uploadImage={uploadImage}
            onPostUpdate={onPostUpdate}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment
                key={comment.id}
                comment={comment}
                onCommentLike={handleCommentLike}
                onCommentDislike={handleCommentDislike}
                onUpdatedCommentToggle={handleUpdatedCommentToggle}
                onDeleteComment={handleDeleteComment}
                onCommentUpdate={handleCommentUpdate}
              />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired,
  onUpdatedPostToggle: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  onDeletePost: PropTypes.func.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired
};

export default ExpandedPost;
