import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Button, Form, Image, SemanticButton } from 'src/components/common/common';
import { postType } from 'src/common/prop-types/prop-types';

import styles from './styles.module.scss';

const UpdatePost = ({ onPostUpdate, uploadImage, postToUpdate }) => {
  const [body, setBody] = React.useState(postToUpdate.body);
  const [image, setImage] = React.useState(postToUpdate.image);
  const [isUploading, setIsUploading] = React.useState(false);

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await onPostUpdate({ imageId: image?.id ?? null, body, id: postToUpdate.id });
  };
  const handleCloseUpdatePost = async () => {
    if (!body) {
      return;
    }
    await onPostUpdate({ imageId: postToUpdate.image?.id, body: postToUpdate.body, id: postToUpdate.id });
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id, link }) => {
        setImage({ id, link });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Form onSubmit={handleUpdatePost}>
      <Form.TextArea
        name="body"
        value={body}
        onChange={ev => setBody(ev.target.value)}
      />
      {image?.link && (
        <div className={styles.imageWrapper}>
          <Image className={styles.image} src={image?.link} alt="post_img" />
        </div>
      )}
      <div className={styles.btnWrapper}>
        <SemanticButton.Group>
          <Button onClick={() => setImage(null)}>Delete image</Button>
          <SemanticButton.Or />
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >
            <label className={styles.btnImgLabel}>
              Attach image
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
        </SemanticButton.Group>
        <SemanticButton.Group>
          <Button onClick={handleCloseUpdatePost}>Cancel</Button>
          <SemanticButton.Or />
          <SemanticButton
            color={ButtonColor.BLUE}
            type={ButtonType.SUBMIT}
          >
            Update
          </SemanticButton>
        </SemanticButton.Group>
      </div>
    </Form>
  );
};

UpdatePost.propTypes = {
  onPostUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  postToUpdate: postType.isRequired
};

export default UpdatePost;
