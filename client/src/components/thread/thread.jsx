import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost } from './components/components';
import { getSortedPosts } from './helpers/helpers';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  isOwnPosts: null,
  isLikedPosts: null
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));

  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showNotOwnPosts, setShowNotOwnPosts] = React.useState(false);
  const [showLikedPosts, setShowLikedPosts] = React.useState(false);
  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(id => {
    dispatch(threadActionCreator.likePost(id));
  }, [dispatch]);

  const handlePostDislike = React.useCallback(id => {
    dispatch(threadActionCreator.dislikePost(id));
  }, [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => {
    dispatch(threadActionCreator.toggleExpandedPost(id));
  }, [dispatch]);

  const handleUpdatedPostToggle = React.useCallback(id => {
    dispatch(threadActionCreator.toggleUpdatedPost(id));
  }, [dispatch]);

  const handleDeletePost = React.useCallback(id => {
    dispatch(threadActionCreator.deletePost(id));
  }, [dispatch]);

  const handlePostAdd = React.useCallback(postPayload => (
    dispatch(threadActionCreator.createPost(postPayload))
  ), [dispatch]);

  const handlePostUpdate = React.useCallback(postPayload => (
    dispatch(threadActionCreator.updatePost(postPayload))
  ), [dispatch]);

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    if (showNotOwnPosts) {
      setShowNotOwnPosts(!showNotOwnPosts);
    }
    if (showLikedPosts) {
      setShowLikedPosts(!showLikedPosts);
    }
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    postsFilter.isOwnPosts = true;
    postsFilter.isLikedPosts = null;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowNotOwnPosts = () => {
    setShowNotOwnPosts(!showNotOwnPosts);
    if (showOwnPosts) {
      setShowOwnPosts(!showOwnPosts);
    }
    if (showLikedPosts) {
      setShowLikedPosts(!showLikedPosts);
    }
    postsFilter.userId = showNotOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    postsFilter.isOwnPosts = false;
    postsFilter.isLikedPosts = null;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    if (showOwnPosts) {
      setShowOwnPosts(!showOwnPosts);
    }
    if (showNotOwnPosts) {
      setShowNotOwnPosts(!showNotOwnPosts);
    }
    postsFilter.userId = showLikedPosts ? undefined : userId;
    postsFilter.from = 0;
    postsFilter.isLikedPosts = true;
    postsFilter.isOwnPosts = null;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          label="Show only not my posts"
          checked={showNotOwnPosts}
          onChange={toggleShowNotOwnPosts}
        />
        <Checkbox
          toggle
          label="Show posts liked by me"
          checked={showLikedPosts}
          onChange={toggleShowOwnLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {getSortedPosts(posts ?? []).map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onUpdatedPostToggle={handleUpdatedPostToggle}
            onDeletePost={handleDeletePost}
            uploadImage={uploadImage}
            onPostUpdate={handlePostUpdate}
            sharePost={sharePost}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
          onUpdatedPostToggle={handleUpdatedPostToggle}
          uploadImage={uploadImage}
          onPostUpdate={handlePostUpdate}
          onDeletePost={handleDeletePost}
          onPostLike={handlePostLike}
          onPostDislike={handlePostDislike}
          onExpandedPostToggle={handleExpandedPostToggle}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
