import { getDiff } from 'src/helpers/helpers';

const getSortedPosts = posts => posts.slice().sort((a, b) => getDiff(b.createdAt, a.createdAt));

export { getSortedPosts };
