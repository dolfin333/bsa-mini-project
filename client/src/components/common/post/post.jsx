import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import UpdatePost from 'src/components/thread/components/update-post/update-post';
import { useSelector } from 'react-redux';
import { Icon, Card, Image, Label, Dropdown, Confirm, Popup, Modal } from 'src/components/common/common';

import styles from './styles.module.scss';

const Post = ({ post, onPostLike, onPostDislike, onExpandedPostToggle,
  onUpdatedPostToggle, onDeletePost, uploadImage, sharePost, onPostUpdate }) => {
  const { userId, postToUpdate } = useSelector(state => ({
    userId: state.profile.user.id,
    postToUpdate: state.posts.postToUpdate
  }));

  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    likeUsers,
    dislikeUsers,
    commentCount,
    createdAt,
    updatedAt
  } = post;
  const date = getFromNowTime(updatedAt);
  const [open, setOpen] = React.useState(false);
  const [openModal, setOpenModal] = React.useState(false);
  const [usersReacted, setUsersReacted] = React.useState([]);

  const show = () => setOpen(true);
  const handleConfirm = () => { setOpen(false); onDeletePost(id); };
  const handleCancel = () => { setOpen(false); };

  const handleOpenUsersLike = () => { setOpenModal(true); setUsersReacted(likeUsers); };
  const handleOpenUsersDislike = () => { setOpenModal(true); setUsersReacted(dislikeUsers); };

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleUpdatedPostToggle = () => onUpdatedPostToggle(id);

  return (
    <Card style={{ width: '100%' }}>
      {((postToUpdate && id !== postToUpdate.id) || !postToUpdate)
        && image && <Image src={image.link} wrapped ui={false} alt="post_img" />}
      <Card.Content>
        <Card.Meta className={styles.meta}>
          <span>
            {updatedAt === createdAt ? 'posted by' : 'updated by'}
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          {userId === user.id && (
            <Dropdown icon={IconName.ELLIPSIS_HORIZONTAL}>
              <Dropdown.Menu>
                <Dropdown.Item text="Update post" onClick={handleUpdatedPostToggle} />
                <Dropdown.Item text="Delete post" onClick={show} />
              </Dropdown.Menu>
            </Dropdown>
          )}
        </Card.Meta>
        <Card.Description>
          {postToUpdate && id === postToUpdate.id
            ? <UpdatePost postToUpdate={postToUpdate} uploadImage={uploadImage} onPostUpdate={onPostUpdate} /> : body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          className={styles.popup}
          trigger={(
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handlePostLike}
            >
              <Icon name={IconName.THUMBS_UP} />
              {likeCount}
            </Label>
          )}
          content={(likeUsers.length !== 0 && (
            <div className={styles.popup_reaction}>
              {likeUsers.slice(-4).map(likeUser => (
                <span key={likeUser.id}>
                  <Image
                    src={likeUser.image?.link ?? DEFAULT_USER_AVATAR}
                    avatar
                    style={{ marginRight: '0' }}
                    alt="user_avatar"
                  />
                  <p>{likeUser.username}</p>
                </span>
              ))}
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={handleOpenUsersLike}
              >
                <Icon name={IconName.PLUS} />
              </Label>
            </div>
          )) || 'no users'}
          basic
          hoverable
        />
        <Popup
          className={styles.popup}
          trigger={(
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handlePostDislike}
            >
              <Icon name={IconName.THUMBS_DOWN} />
              {dislikeCount}
            </Label>
          )}
          content={(dislikeUsers.length !== 0 && (
            <div className={styles.popup_reaction}>
              {dislikeUsers.map(dislikeUser => (
                <span key={dislikeUser.id}>
                  <Image
                    src={dislikeUser.image?.link ?? DEFAULT_USER_AVATAR}
                    style={{ margin: '5px' }}
                    avatar
                    alt="user_avatar"
                  />
                  <p>{dislikeUser.username}</p>
                </span>
              ))}
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={handleOpenUsersDislike}
              >
                <Icon name={IconName.PLUS} />
              </Label>
            </div>
          )) || 'no users'}
          basic
          hoverable
        />
        <Modal
          dimmer="blurring"
          centered={false}
          open={openModal}
          onClose={() => setOpenModal(false)}
        >
          {usersReacted && (
            <Modal.Content>
              {usersReacted.map(userReacted => (
                <span key={userReacted.id} className={styles.modal_info}>
                  <Image
                    src={userReacted.image?.link ?? DEFAULT_USER_AVATAR}
                    size="tiny"
                    circular
                    style={{ margin: '5px' }}
                    alt="user_img"
                  />
                  <span>
                    <span className={styles.modal_user_info}>
                      {userReacted.username}
                      <span style={{ color: 'gray' }}>{userReacted.status}</span>
                    </span>
                  </span>
                </span>
              ))}
            </Modal.Content>
          )}
        </Modal>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>

        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
      </Card.Content>
      <Confirm
        open={open}
        header="Post deleting"
        content="Are you sure that you want to delete this post?"
        confirmButton="Delete"
        onCancel={handleCancel}
        onConfirm={handleConfirm}
      />
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  onDeletePost: PropTypes.func.isRequired,
  onUpdatedPostToggle: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
