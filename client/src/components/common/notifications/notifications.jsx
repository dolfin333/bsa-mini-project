import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import io from 'socket.io-client';
import {
  NotificationContainer,
  NotificationManager
} from 'react-notifications';
import { ENV } from 'src/common/enums/enums';
import { userType } from 'src/common/prop-types/prop-types';
import { profileActionCreator } from 'src/store/actions';

import 'react-notifications/lib/notifications.css';

const socket = io(ENV.SOCKET_URL);

const Notifications = ({ user, onPostApply, onDeletePostApply, onUpdatePostApply }) => {
  const { leaveRoomId } = useSelector(state => ({
    leaveRoomId: state.profile.leaveRoomId
  }));
  const dispatch = useDispatch();

  const handleLeaveRoomId = React.useCallback(() => (
    dispatch(profileActionCreator.setLeaveRoomId(''))
  ), [dispatch]);

  React.useEffect(() => {
    if (leaveRoomId !== '') {
      socket.emit('leaveRoom', leaveRoomId);
      handleLeaveRoomId();
    }
    if (!user) {
      return undefined;
    }
    socket.emit('createRoom', user.id);
    socket.on('like', mess => {
      NotificationManager.info(mess);
    });
    socket.on('dislike', mess => {
      NotificationManager.info(mess);
    });
    socket.on('new_post', post => {
      if (post.userId !== user.id) {
        onPostApply(post.id);
      }
    });
    socket.on('delete_post', post => {
      if (post.userId !== user.id) {
        onDeletePostApply(post.id);
      }
    });
    socket.on('update_post', post => {
      if (post.userId !== user.id) {
        onUpdatePostApply(post);
      }
    });
    return () => {
      socket.close();
    };
  }, [user]);

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: userType,
  onPostApply: PropTypes.func.isRequired,
  onDeletePostApply: PropTypes.func.isRequired,
  onUpdatePostApply: PropTypes.func.isRequired
};

export default Notifications;
