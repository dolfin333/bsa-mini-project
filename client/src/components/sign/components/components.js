import LoginForm from './login-form/login-form';
import RegistrationForm from './registration-form/registration-form';
import SendEmailForm from './send-email-form/send-email-form';
import ChangePasswordForm from './change-password-form/change-password-form';

export { LoginForm, RegistrationForm, SendEmailForm, ChangePasswordForm };
