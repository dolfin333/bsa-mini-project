import * as React from 'react';
import validator from 'validator';
import { send } from 'emailjs-com';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  ENV
  // AppRoute
} from 'src/common/enums/enums';
import {
  Button,
  Form,
  Segment,
  Message
  // NavLink
} from 'src/components/common/common';
import styles from './styles.module.scss';

const SendEmailForm = () => {
  const [email, setEmail] = React.useState('');
  const [isLoading, setLoading] = React.useState(false);
  const [isEmailValid, setIsEmailValid] = React.useState(true);
  const [mess, setMess] = React.useState('');

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleSendEmailClick = e => {
    e.preventDefault();
    setLoading(true);
    send(
      ENV.SERVICE_ID,
      ENV.TEMPLATE_ID,
      { link: `${window.location.origin}/resetPassword`, email },
      ENV.USER_ID
    )
      .then(() => {
        // console.log('SUCCESS!', response.status, response.text);
        setMess('Check you email for the link!');
      })
      .catch(() => {
        // console.log('FAILED...', err);
      });
    setLoading(false);
  };

  return (
    <>
      <h2 className={styles.title}>Enter your email</h2>
      <Form name="sendEmailForm" size="large" onSubmit={handleSendEmailClick}>
        <Segment>
          <Form.Input
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            error={!isEmailValid}
            onChange={ev => emailChanged(ev.target.value)}
            onBlur={() => setIsEmailValid(validator.isEmail(email))}
          />
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.TEAL}
            size={ButtonSize.LARGE}
            isLoading={isLoading}
            isFluid
            isPrimary
          >
            Send link
          </Button>
        </Segment>
      </Form>
      {mess !== '' && (
        <Message>
          {mess}
        </Message>
      )}
    </>
  );
};

export default SendEmailForm;
