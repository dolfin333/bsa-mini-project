import * as React from 'react';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute
} from 'src/common/enums/enums';
import {
  Button,
  Form,
  Segment,
  Message,
  NavLink
} from 'src/components/common/common';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

const ChangePasswordForm = ({ changePassword }) => {
  const [password, setPassword] = React.useState('');
  const [isPasswordValid, setIsPasswordValid] = React.useState(true);
  const [isUsernameValid, setUsernameValid] = React.useState(true);
  const [username, setUsername] = React.useState('');
  const [errMess, setErrMess] = React.useState('');
  const history = useHistory();

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const usernameChanged = value => {
    setUsername(value);
    setUsernameValid(true);
  };

  const handleChangePasswordClick = async () => {
    try {
      await changePassword({ password, username });
      history.push('/login');
    } catch (err) {
      setUsernameValid(false);
      setErrMess('No user with such name');
    }
  };

  return (
    <>
      <h2 className={styles.title}>Change your password</h2>
      <Form name="loginForm" size="large" onSubmit={handleChangePasswordClick}>
        <Segment>
          <Form.Input
            fluid
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            error={!isUsernameValid && { content: errMess, pointing: 'below' }}
            onChange={ev => usernameChanged(ev.target.value)}
            onBlur={() => setUsernameValid(Boolean(username))}
          />
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            error={!isPasswordValid}
            onChange={ev => passwordChanged(ev.target.value)}
            onBlur={() => setIsPasswordValid(Boolean(password))}
          />
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.TEAL}
            size={ButtonSize.LARGE}
            // isLoading={isLoading}
            isFluid
            isPrimary
          >
            Change password
          </Button>
        </Segment>
      </Form>
      <Message>
        New to us?
        {' '}
        <NavLink exact to={AppRoute.REGISTRATION}>
          Sign Up
        </NavLink>
      </Message>
    </>
  );
};

ChangePasswordForm.propTypes = {
  changePassword: PropTypes.func.isRequired
};

export default ChangePasswordForm;
