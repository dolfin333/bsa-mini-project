import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, Image, Input, Button } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { profileActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import UpdateProfile from 'src/components/profile/update-profile/update-profile';
import styles from './styles.module.scss';

const Profile = () => {
  const { user, userToUpdate } = useSelector(state => ({
    user: state.profile.user,
    userToUpdate: state.profile.userToUpdate
  }));
  const { id } = user;
  const dispatch = useDispatch();

  const handleUpdatedUserToggle = React.useCallback(userId => {
    dispatch(profileActionCreator.toggleUpdatedProfile(userId));
  }, [dispatch]);

  const handleUserUpdate = React.useCallback(userPayload => (
    dispatch(profileActionCreator.updateProfile(userPayload))
  ), [dispatch]);

  const uploadImage = file => imageService.uploadImage(file);

  const onUpdatedUserToggle = () => handleUpdatedUserToggle(id);

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <div className={styles.profileContent}>
        <Grid.Column>
          {!userToUpdate
            && (
              <div>
                <Image
                  centered
                  src={user.image?.link ?? DEFAULT_USER_AVATAR}
                  size="medium"
                  circular
                  alt="user_img"
                />
                <br />
                <Input
                  icon="user"
                  iconPosition="left"
                  placeholder="Username"
                  type="text"
                  disabled
                  value={user.username}
                />
                <br />
                <br />
                <Input
                  icon="at"
                  iconPosition="left"
                  placeholder="Email"
                  type="email"
                  disabled
                  value={user.email}
                />
                <br />
                <br />
                <Input
                  icon="address card outline"
                  iconPosition="left"
                  placeholder="Status"
                  type="text"
                  disabled
                  value={user.status ? user.status : ''}
                />
                <br />
                <br />
                <Button color="teal" onClick={onUpdatedUserToggle}>Update profile</Button>
              </div>
            )}
          {userToUpdate
            && <UpdateProfile onUserUpdate={handleUserUpdate} uploadImage={uploadImage} userToUpdate={userToUpdate} />}
        </Grid.Column>
      </div>
    </Grid>
  );
};

export default Profile;
