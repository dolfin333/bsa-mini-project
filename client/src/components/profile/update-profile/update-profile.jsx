import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Button, Form, Image, SemanticButton } from 'src/components/common/common';
import { profileActionCreator } from 'src/store/actions';
import { userType } from 'src/common/prop-types/prop-types';
import validator from 'validator';
// import ReactCrop from 'react-image-crop';

import 'react-image-crop/dist/ReactCrop.css';
import styles from './styles.module.scss';

const UpdateProfile = ({ onUserUpdate, uploadImage, userToUpdate }) => {
  const [username, setUsername] = React.useState(userToUpdate.username);
  const [isUsernameValid, setUsernameValid] = React.useState(true);
  const [isEmailValid, setEmailValid] = React.useState(true);
  const [email, setEmail] = React.useState(userToUpdate.email);
  const [errMess, setErrMess] = React.useState('');
  const [status, setStatus] = React.useState(userToUpdate.status);
  const [image, setImage] = React.useState(userToUpdate.image);
  const [isUploading, setIsUploading] = React.useState(false);
  // const [crop, setCrop] = React.useState({ aspect: 16 / 9, maxHeight: 200, maxWidth: 200 });
  // const [open, setOpen] = React.useState(false);

  const emailChanged = value => {
    setEmail(value);
    setEmailValid(true);
  };

  const usernameChanged = value => {
    setUsername(value);
    setUsernameValid(true);
  };

  const handleUpdateUser = async () => {
    if (!(username && email)) {
      return;
    }
    if (await profileActionCreator.checkIfUsernameExists(username) && username !== userToUpdate.username) {
      setUsernameValid(false);
      setErrMess('User with such username is already exists');
      return;
    }
    await onUserUpdate({ imageId: image?.id ?? null, username, email, status, id: userToUpdate.id });
  };

  const handleCloseUpdateUser = async () => {
    if (!(username && email)) {
      return;
    }
    await onUserUpdate({
      imageId: userToUpdate.image?.id,
      email: userToUpdate.email,
      username: userToUpdate.username,
      status: userToUpdate.status,
      id: userToUpdate.id
    });
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;
    uploadImage(file)
      .then(({ id, link }) => {
        setImage({ id, link });
        // setOpen(true);
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  // const handleImageLoaded = imagee => {
  //   console.log(imagee);
  // };

  // const handleOnCropChange = newCrop => {
  //   setCrop(newCrop);
  // };
  // const handleOnCropComplete = (newCrop, pixelCrop) => {
  //   setCrop(newCrop);
  //   console.log(pixelCrop);
  // };

  return (
    <Form onSubmit={handleUpdateUser}>
      <Form.Input
        fluid
        icon="user"
        iconPosition="left"
        placeholder="Username"
        type="text"
        value={username}
        error={!isUsernameValid && { content: errMess, pointing: 'below' }}
        onChange={ev => usernameChanged(ev.target.value)}
        onBlur={() => setUsernameValid(Boolean(username))}
      />
      <Form.Input
        fluid
        icon="at"
        iconPosition="left"
        placeholder="Email"
        type="email"
        value={email}
        error={!isEmailValid}
        onChange={ev => emailChanged(ev.target.value)}
        onBlur={() => setEmailValid(validator.isEmail(email))}
      />
      <Form.TextArea
        name="status"
        value={status || ''}
        onChange={ev => setStatus(ev.target.value)}
      />
      {image?.link && (
        <div className={styles.imageWrapper}>
          <Image className={styles.image} src={image?.link} alt="post_img" />
        </div>
      )}
      <div className={styles.btnWrapper}>
        <SemanticButton.Group>
          <Button onClick={() => setImage(null)}>Delete avatar</Button>
          <SemanticButton.Or />
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >
            <label className={styles.btnImgLabel}>
              Attach avatar
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
        </SemanticButton.Group>
        <SemanticButton.Group>
          <Button onClick={handleCloseUpdateUser}>Cancel</Button>
          <SemanticButton.Or />
          <SemanticButton
            color={ButtonColor.BLUE}
            type={ButtonType.SUBMIT}
          >
            Update
          </SemanticButton>
        </SemanticButton.Group>
      </div>
      {/* <Modal
        // onClose={() => setOpen(false)}
        // onOpen={() => setOpen(true)}
        closeOnDimmerClick
        open={open}
      >
        <Modal.Header>Select a Photo</Modal.Header>
        <ReactCrop
          src={image?.link}
          crop={crop}
          onChange={handleOnCropChange}
          onImageLoaded={handleImageLoaded}
          onComplete={handleOnCropComplete}
        />
        <Modal.Actions>
          <Button onClick={() => setOpen(false)}>
            Close
          </Button>
          <Button
            content="Crop"
            onClick={() => setOpen(false)}
            positive
          >
            Crop
          </Button>
        </Modal.Actions>
      </Modal> */}
    </Form>
  );
};

UpdateProfile.propTypes = {
  onUserUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  userToUpdate: userType.isRequired
};

export default UpdateProfile;
