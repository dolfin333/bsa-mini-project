import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  UPDATE_POST: 'thread/update-post',
  UPDATE_COMMENT: 'thread/update-comment',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_UPDATED_POST: 'thread/set-updated-post',
  SET_UPDATED_COMMENT: 'thread/set-updated-comment',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  SET_EXPANDED_POST_COMMENTS: 'thread/set-expanded-post-comments'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const setComment = createAction(ActionType.SET_COMMENT, comment => ({
  payload: {
    comment
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const addUpdatePost = createAction(ActionType.UPDATE_POST, post => ({
  payload: {
    post
  }
}));

const addUpdateComment = createAction(ActionType.UPDATE_COMMENT, comment => ({
  payload: {
    comment
  }
}));

const setUpdatedPost = createAction(ActionType.SET_UPDATED_POST, post => ({
  payload: {
    post
  }
}));

const setUpdatedComment = createAction(ActionType.SET_UPDATED_COMMENT, comment => ({
  payload: {
    comment
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPostComments = createAction(ActionType.SET_EXPANDED_POST_COMMENTS, comments => ({
  payload: {
    comments
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const applyUpdatePost = post => async (dispatch, getRootState) => {
  const {
    posts: { expandedPost }
  } = getRootState();
  // eslint-disable-next-line eqeqeq
  const { id } = await postService.updatePost(post, post.id);
  const newPost = await postService.getPost(id);
  dispatch(addUpdatePost(newPost));
  dispatch(setUpdatedPost(undefined));
  if (expandedPost) { dispatch(setExpandedPost(newPost)); }
};

const updatePost = post => async (dispatch, getRootState) => {
  const {
    posts: { expandedPost, postToUpdate }
  } = getRootState();
  let newPost = postToUpdate;
  // eslint-disable-next-line eqeqeq
  if (!(post.body === postToUpdate.body && post.imageId == postToUpdate.imageId)) {
    const { id } = await postService.updatePost(post, post.id);
    newPost = await postService.getPost(id);
    dispatch(addUpdatePost(newPost));
  }
  dispatch(setUpdatedPost(undefined));
  if (expandedPost) { dispatch(setExpandedPost(newPost)); }
};

const updateComment = comment => async (dispatch, getRootState) => {
  const {
    posts: { commentToUpdate }
  } = getRootState();
  let newPost = commentToUpdate;
  // eslint-disable-next-line eqeqeq
  if (!(comment.body === commentToUpdate.body)) {
    const { id } = await commentService.updateComment(comment, comment.id);
    newPost = await commentService.getComment(id);
    dispatch(addUpdateComment(newPost));
  }
  dispatch(setUpdatedComment(undefined));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
  if (post) {
    dispatch(setExpandedPostComments(post.comments));
  }
};

const applyDeletePost = postId => async (dispatch, getRootState) => {
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updatedPosts = posts.filter(post => post.id !== postId);
  if (expandedPost) { dispatch(setExpandedPost(null)); }
  dispatch(setPosts(updatedPosts));
};

const deletePost = postId => async (dispatch, getRootState) => {
  await postService.deletePost(postId);
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updatedPosts = posts.filter(post => post.id !== postId);
  if (expandedPost) { dispatch(setExpandedPost(null)); }
  dispatch(setPosts(updatedPosts));
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  await commentService.deleteComment(commentId);
  const {
    posts: { expandedPostComments, expandedPost }
  } = getRootState();
  const updatedComments = expandedPostComments.filter(comment => comment.id !== commentId);
  dispatch(setExpandedPostComments(updatedComments));
  const post = await postService.getPost(expandedPost.id);
  dispatch(setExpandedPost(post));
  dispatch(addUpdatePost(post));
};

const toggleUpdatedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setUpdatedPost(post));
};

const toggleUpdatedComment = commentId => async dispatch => {
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  dispatch(setUpdatedComment(comment));
};

function changeDislikeCount(dispatch, getRootState, diff, postId) {
  const {
    posts: { posts, expandedPost },
    profile: { user }
  } = getRootState();

  const mapDislikes = post => {
    let dislikeUsers = [...post.dislikeUsers];
    if (diff === 1) {
      const dislikeUser = {
        id: user.id,
        username: user.username,
        image: { id: user.image?.id, link: user.image?.link },
        status: user.status
      };
      dislikeUsers.push(dislikeUser);
    } else {
      dislikeUsers = dislikeUsers.filter(dislikeUser => dislikeUser.userId ?? dislikeUser.id !== user.id);
    }
    return {
      ...post,
      dislikeCount: (Number(post.dislikeCount) + diff).toString(), // diff is taken from the current closure
      dislikeUsers
    };
  };

  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapDislikes(expandedPost)));
  }
}

function changeLikeCount(dispatch, getRootState, diff, postId) {
  const {
    posts: { posts, expandedPost },
    profile: { user }
  } = getRootState();
  const mapLikes = post => {
    let likeUsers = [...post.likeUsers];
    if (diff === 1) {
      const likeUser = {
        id: user.id,
        username: user.username,
        image: { id: user.image?.id, link: user.image?.link },
        status: user.status
      };
      likeUsers.push(likeUser);
    } else {
      likeUsers = likeUsers.filter(likeUser => likeUser.userId ?? likeUser.id !== user.id);
    }
    return {
      ...post,
      likeCount: (Number(post.likeCount) + diff).toString(), // diff is taken from the current closure
      likeUsers
    };
  };

  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));
  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
}

const likePost = postId => async (dispatch, getRootState) => {
  const res = await postService.likePost(postId);
  if (res.updatedAt !== res.createdAt) {
    changeDislikeCount(dispatch, getRootState, -1, postId);
  }
  const diff = res.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  changeLikeCount(dispatch, getRootState, diff, postId);
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const res = await postService.dislikePost(postId);
  if (res.updatedAt !== res.createdAt) {
    changeLikeCount(dispatch, getRootState, -1, postId);
  }
  const diff = res.id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed

  changeDislikeCount(dispatch, getRootState, diff, postId);
};

function changeDislikeCountComment(dispatch, getRootState, diff, commentId) {
  const {
    posts: { expandedPostComments },
    profile: { user }
  } = getRootState();
  const mapDislikes = comment => {
    let dislikeUsersComment = [...comment.dislikeUsersComment];
    if (diff === 1) {
      const dislikeUserComment = {
        id: user.id,
        username: user.username,
        image: { id: user.image?.id, link: user.image?.link },
        status: user.status
      };
      dislikeUsersComment.push(dislikeUserComment);
    } else {
      dislikeUsersComment = dislikeUsersComment.filter(dislikeUserComment => dislikeUserComment.id !== user.id);
    }
    return {
      ...comment,
      dislikeCountComment: (Number(comment.dislikeCountComment) + diff).toString(),
      // diff is taken from the current closure
      dislikeUsersComment
    };
  };

  const updated = expandedPostComments.map(comment => (comment.id !== commentId ? comment : mapDislikes(comment)));
  dispatch(setExpandedPostComments(updated));
}

function changeLikeCountComment(dispatch, getRootState, diff, commentId) {
  const {
    posts: { expandedPostComments },
    profile: { user }
  } = getRootState();
  const mapLikes = comment => {
    let likeUsersComment = [...comment.likeUsersComment];
    if (diff === 1) {
      const likeUserComment = {
        id: user.id,
        username: user.username,
        image: { id: user.image?.id || null, link: user.image?.link || null },
        status: user.status
      };
      likeUsersComment.push(likeUserComment);
    } else {
      likeUsersComment = likeUsersComment.filter(likeUserComment => likeUserComment.id !== user.id);
    }
    return {
      ...comment,
      likeCountComment: (Number(comment.likeCountComment) + diff).toString(),
      // diff is taken from the current closure
      likeUsersComment
    };
  };

  const updated = expandedPostComments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));
  dispatch(setExpandedPostComments(updated));
}

const likeComment = commentId => async (dispatch, getRootState) => {
  const res = await commentService.likeComment(commentId);
  if (res.updatedAt !== res.createdAt) {
    changeDislikeCountComment(dispatch, getRootState, -1, commentId);
  }
  const diff = res.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  changeLikeCountComment(dispatch, getRootState, diff, commentId);
};

const dislikeComment = commentId => async (dispatch, getRootState) => {
  const res = await commentService.dislikeComment(commentId);
  if (res.updatedAt !== res.createdAt) {
    changeLikeCountComment(dispatch, getRootState, -1, commentId);
  }
  const diff = res.id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
  changeDislikeCountComment(dispatch, getRootState, diff, commentId);
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);
  const {
    posts: { expandedPostComments, expandedPost }
  } = getRootState();
  const updatedComments = Array.from(expandedPostComments);
  updatedComments.push(comment);
  dispatch(setExpandedPostComments(updatedComments));
  const post = await postService.getPost(expandedPost.id);
  dispatch(setExpandedPost(post));
  dispatch(addUpdatePost(post));
};

export {
  setPosts,
  addMorePosts,
  addPost,
  addUpdatePost,
  addUpdateComment,
  setUpdatedPost,
  setUpdatedComment,
  setExpandedPost,
  setComment,
  setExpandedPostComments,
  loadPosts,
  loadMorePosts,
  updatePost,
  updateComment,
  applyPost,
  applyDeletePost,
  applyUpdatePost,
  createPost,
  deletePost,
  deleteComment,
  toggleUpdatedPost,
  toggleExpandedPost,
  toggleUpdatedComment,
  likePost,
  dislikePost,
  likeComment,
  dislikeComment,
  addComment
};
