import { createReducer } from '@reduxjs/toolkit';
import {
  setPosts, addMorePosts, addPost, setExpandedPost,
  setExpandedPostComments, setComment, setUpdatedPost,
  addUpdatePost, setUpdatedComment, addUpdateComment
} from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  postToUpdate: null,
  commentToUpdate: null,
  hasMorePosts: true,
  expandedPostComments: []
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setPosts, (state, action) => {
    const { posts } = action.payload;
    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addMorePosts, (state, action) => {
    const { posts } = action.payload;
    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addPost, (state, action) => {
    const { post } = action.payload;
    state.posts = [post, ...state.posts];
  });
  builder.addCase(addUpdatePost, (state, action) => {
    const { post } = action.payload;
    state.posts = state.posts.filter(statePost => statePost.id !== post.id);
    state.posts = [post, ...state.posts];
  });
  builder.addCase(addUpdateComment, (state, action) => {
    const { comment } = action.payload;
    state.expandedPostComments = state.expandedPostComments.filter(stateComment => stateComment.id !== comment.id);
    state.expandedPostComments = [comment, ...state.expandedPostComments];
  });
  builder.addCase(setUpdatedPost, (state, action) => {
    const { post } = action.payload;
    state.postToUpdate = post;
  });
  builder.addCase(setUpdatedComment, (state, action) => {
    const { comment } = action.payload;
    state.commentToUpdate = comment;
  });
  builder.addCase(setExpandedPost, (state, action) => {
    const { post } = action.payload;
    state.expandedPost = post;
  });
  builder.addCase(setExpandedPostComments, (state, action) => {
    const { comments } = action.payload;
    state.expandedPostComments = comments;
  });
  builder.addCase(setComment, (state, action) => {
    const { comment } = action.payload;
    state.comment = comment;
  });
});

export { reducer };
