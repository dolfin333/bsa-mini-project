import { createReducer } from '@reduxjs/toolkit';
import { setUser, setUpdatedUser, setUsernameUpdatePass, setLeaveRoomId } from './actions';

const initialState = {
  user: null,
  userToUpdate: null,
  usernameUpdatePass: '',
  leaveRoomId: ''
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setUser, (state, action) => {
    const { user } = action.payload;
    state.user = user;
  });
  builder.addCase(setLeaveRoomId, (state, action) => {
    const { roomId } = action.payload;
    state.leaveRoomId = roomId;
  });
  builder.addCase(setUpdatedUser, (state, action) => {
    const { user } = action.payload;
    state.userToUpdate = user;
  });
  builder.addCase(setUsernameUpdatePass, (state, action) => {
    const { username } = action.payload;
    state.usernameUpdatePass = username;
  });
});

export { reducer };
