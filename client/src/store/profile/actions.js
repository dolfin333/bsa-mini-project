import { createAction } from '@reduxjs/toolkit';
import { StorageKey } from 'src/common/enums/enums';
import {
  storage as storageService,
  auth as authService
} from 'src/services/services';

const ActionType = {
  SET_USER: 'profile/set-user',
  SET_UPDATED_USER: 'profile/set-updated-user',
  SET_USERNAME_UPDATE_PASS: 'profile/set-username-update-pass',
  SET_LEAVE_ROOM_ID: 'profile/set-leave-room-id'
};

const setUser = createAction(ActionType.SET_USER, user => ({
  payload: {
    user
  }
}));

const setLeaveRoomId = createAction(ActionType.SET_LEAVE_ROOM_ID, roomId => ({
  payload: {
    roomId
  }
}));

const setUpdatedUser = createAction(ActionType.SET_UPDATED_USER, user => ({
  payload: {
    user
  }
}));

const setUsernameUpdatePass = createAction(ActionType.SET_USERNAME_UPDATE_PASS, username => ({
  payload: {
    username
  }
}));

const login = request => async dispatch => {
  const { user, token } = await authService.login(request);

  storageService.setItem(StorageKey.TOKEN, token);
  setLeaveRoomId('');
  dispatch(setUser(user));
};

const register = request => async dispatch => {
  const { user, token } = await authService.registration(request);

  storageService.setItem(StorageKey.TOKEN, token);
  setLeaveRoomId('');
  dispatch(setUser(user));
};

const checkIfUsernameExists = async username => {
  const user = await authService.getUserByUsername(username);
  if (Object.keys(user).length === 0) {
    return false;
  }
  return true;
};

const changePassword = request => async dispatch => {
  await authService.changePassword(request);
  dispatch(setUser(null));
};

const logout = () => (dispatch, getRootState) => {
  const {
    profile: { user }
  } = getRootState();
  dispatch(setLeaveRoomId(user.id));
  storageService.removeItem(StorageKey.TOKEN);
  dispatch(setUser(null));
};

const loadCurrentUser = () => async dispatch => {
  const user = await authService.getCurrentUser();
  dispatch(setUser(user));
};

const toggleUpdatedProfile = userId => async dispatch => {
  const user = userId ? await authService.getUser(userId) : undefined;
  dispatch(setUpdatedUser(user));
};

const updateProfile = user => async (dispatch, getRootState) => {
  const {
    profile: { userToUpdate }
  } = getRootState();
  let newPost = userToUpdate;
  if (!(user.username === userToUpdate.username && user.email === userToUpdate.email
    // eslint-disable-next-line eqeqeq
    && user.status === userToUpdate.status && user.imageId == userToUpdate.image?.id)) {
    const { id } = await authService.updateUser(user, user.id);
    newPost = await authService.getUser(id);
    dispatch(setUser(newPost));
  }
  dispatch(setUpdatedUser(undefined));
};

export {
  setUser, setUpdatedUser, setUsernameUpdatePass, setLeaveRoomId, login, register, logout, loadCurrentUser,
  toggleUpdatedProfile, updateProfile, changePassword, checkIfUsernameExists
};
