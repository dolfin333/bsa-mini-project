const AppRoute = {
  ROOT: '/',
  ANY: '*',
  LOGIN: '/login',
  REGISTRATION: '/registration',
  RESET_PASSWORD: '/resetPassword',
  SEND_EMAIL: '/sendEmail',
  PROFILE: '/profile',
  SHARE_$POSTHASH: '/share/:postHash'
};

export { AppRoute };
