const IconName = {
  USER_CIRCLE: 'user circle',
  LOG_OUT: 'log out',
  THUMBS_UP: 'thumbs up',
  THUMBS_DOWN: 'thumbs down',
  COMMENT: 'comment',
  SHARE_ALTERNATE: 'share alternate',
  FROWN: 'frown',
  IMAGE: 'image',
  COPY: 'copy',
  CHEVRON_DOWN: 'chevron down',
  ELLIPSIS_HORIZONTAL: 'ellipsis horizontal',
  PLUS: 'plus'
};

export { IconName };
